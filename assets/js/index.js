/* globals Core, M, Form, ui, Fecha, Ax, u, Ic, Dropdown, z */
var idea;
$(document).ready(function () {
    idea = new Idea();
    setTimeout(() => {
        $('.loading').fadeOut(250);
        $('.tabs').tabs('updateTabIndicator');
        $('.slider').slider({
            indicators: false,
            height: 550
        });
        $(".dropdown-trigger").dropdown({
            constrainWidth: false,
            hover: true,
            // coverTrigger: false,
            container: $('.app')
        });
        idea.home();
    }, 1000);
});

class Idea {
    constructor() {
        $('.tabs').tabs();
        $('.sidenav').sidenav();
        $('.collapsible').collapsible();
        $('.modal').modal();
        $('.materialboxed').materialbox();
        (new Fecha()).Dtpicker('#a_birthday');
        $('select').formSelect();
        $.cookie('set', 'false');
        this.clicks();
    }
    clicks() {
        u.click('.educative_offer', this.offer);
        u.click('.between_us', this.us);
        u.click('.student_services', this.student_services);
        u.click('.oedu', this.offer);
        u.click('.posg', this.posg);
        u.click('.dipl', this.dipl);
        u.click('.services', this.services);
        u.click('.talleres', this.talleres);
        u.click('.admisiones', this.admisiones);
        u.click('.nosotros', this.nosotros);
        u.click('.contacto', this.contacto);
        u.click('.home', this.home);
        u.click('.send_meeting', this.send_meeting);
        u.click('.send_admission_request', this.send_admission_request);
        u.click('.b.card', this.openSection);
        u.click('.returnToList', this.returnToList);
        u.click('.polarizado', this.polarizado);
    }
    returnToList(v) {
        $('.hiddable').slideDown(350);
        $('.' + v.attr('data-open')).slideUp(350);
    }
    openSection(v) {
        $('.hiddable').slideUp(350);
        $('.' + v.attr('data-open')).slideDown(350);
        $("html, body").stop().animate({ scrollTop: 530 }, 500, 'swing');
    }
    send_meeting() {
        let data = {
            first_name: $('#first_name').val(),
            email: $('#email').val(),
            phone_number: $('#phone_number').val()
        }
        if (data.first_name != '' && data.email != '' && data.phone_number != '') {
            $('.send_meeting').addClass('disabled').html('Enviando correo...');
            $.post('email_services/', { data: data }, (r) => {
                r = JSON.parse(r);
                $('.send_meeting').removeClass('disabled').html('Solicitar informes');
                switch (r.response) {
                    case 'true':
                        $('.modal').modal('close');
                        Core.toast('Correo electrónico enviado. Recibirá una llamada en menos de 24 horas.', 'green');
                        break;
                    default:
                        Core.toast('Error al enviar el correo electrónico, intente nuevamente.', 'red');
                        break;
                }
            });
        }
    }
    home() {
        $('body').addClass('grey').removeClass('white');
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        $('.c').removeClass('scale-in');
        $('.d').removeClass('scale-in');
        $('.e').removeClass('scale-in');
        setTimeout(() => {
            $('.c').addClass('scale-in');
            setTimeout(() => {
                $('.d').addClass('scale-in');
                setTimeout(() => {
                    $('.e').addClass('scale-in');
                }, 201);
            }, 201);
        }, 201);
    }
    services() {////
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        $('.slider.database').slider({
            indicators: false,
            height: 450
        });
        $('.a').removeClass('scale-in');
        $('.b').removeClass('scale-in');
        setTimeout(() => {
            $('.a').addClass('scale-in');
            setTimeout(() => {
                $('.b').addClass('scale-in');
            }, 201);
        }, 201);
        $.cookie('posgrado', 'null')
        $.cookie('licenciaturas', 'null')
    }
    talleres() {
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        $('.parallax9').parallax();
        $('.aa').removeClass('scale-in');
        $('.bb').removeClass('scale-in');
        setTimeout(() => {
            $('.aa').addClass('scale-in');
            setTimeout(() => {
                $('.bb').addClass('scale-in');
            }, 201);
        }, 201);
        $.cookie('posgrado', 'null')
        $.cookie('licenciaturas', 'null')
    }
    admisiones() {
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        $('.parallax3').parallax();
        $('.h').removeClass('scale-in');
        $('.i').removeClass('scale-in');
        setTimeout(() => {
            $('.h').addClass('scale-in');
            setTimeout(() => {
                $('.i').addClass('scale-in');
            }, 201);
        }, 201);
        $.cookie('posgrado', 'null')
        $.cookie('licenciaturas', 'null')
    }
    nosotros() {
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        // $('.parallax4').parallax();
        $('.j').removeClass('scale-in');
        $('.k').removeClass('scale-in');
        $('.l').removeClass('scale-in');
        setTimeout(() => {
            $('.j').addClass('scale-in');
            setTimeout(() => {
                $('.k').addClass('scale-in');
                setTimeout(() => {
                    $('.l').addClass('scale-in');
                }, 201);
            }, 201);
        }, 201);
        $.cookie('posgrado', 'null')
        $.cookie('licenciaturas', 'null')
    }
    contacto() {
        $('body').addClass('grey').removeClass('white');
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        $.cookie('posgrado', 'null')
        $.cookie('licenciaturas', 'null')
    }
    polarizado(v){
        let attr = v.attr('data-target');
        $('.tabs').tabs('select', 'test4');
        $('.slide_s').hide(0);
        $('.'+attr).show(0);
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        console.log(attr);
    }
    offer(v) { //test4
        $('.slider.database').slider({
            indicators: false,
            height: 450
        });
        $('.a').removeClass('scale-in');
        $('.b').removeClass('scale-in');
        setTimeout(() => {
            $('.a').addClass('scale-in');
            setTimeout(() => {
                $('.b').addClass('scale-in');
            }, 201);
        }, 201);
        $.cookie('posgrado', 'null');
        // $('.carousel.carousel-slider').carousel({
        //     fullWidth: true,
        //     indicators: true
        // });
    }
    posg(v) {
        let attr;
        if (M.Tabs.getInstance($('.tabs')).index != 2) {
            attr = v[0].hasAttribute('data-target') ? v.attr('data-target') : '';
            $('.tabs').tabs('select', 'test7');
            $.cookie('posgrado', attr);
        } else {
            if ($.cookie('posgrado') != 'null') {
                let attr2 = v[0].hasAttribute('data-target') ? v.attr('data-target') : '';
                console.log('JI')
                attr = $.cookie('posgrado');
                if (attr != attr2 && attr2 != 'posgrado') {
                    attr = attr2;
                }
                $.cookie('posgrado', 'null');
            } else {
                console.log('NO')
                attr = v[0].hasAttribute('data-target') ? v.attr('data-target') : '';
            }
        }
        $('.a').removeClass('scale-in');
        $('.b.card').removeClass('scale-in');
        setTimeout(() => {
            $('.a').addClass('scale-in');
            setTimeout(() => {
                $('.b.card').addClass('scale-in');
            }, 201);
        }, 201);
        if (attr != '') {
            switch (attr) {
                case 'posgrado':
                case 'posgrados':
                    $('.returnToList').click();
                    $("html, body").stop().animate({ scrollTop: 550 }, 500, 'swing');
                    break;
                case 'administracion_negocios':
                case 'derecho_privado':
                    $('.administracion_negocios, .derecho_privado').slideUp(0);
                    $('.b.card[data-open="' + attr + '"]').click();
                    break;
            }
            $.cookie('set', 'true');
            console.log(M.Tabs.getInstance($('.tabs')).index);
            if (M.Tabs.getInstance($('.tabs')).index != 2)
                $('.tabs').tabs('select', 'test7');
        } else {
            if ($.cookie('set') != 'true') {
                $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
            } else
                $.cookie('set', 'false');
        }
        $('.parallax1').parallax();
        $('.carousel').carousel({
            dist: 0,
            numVisible: 3
        });
        $.cookie('licenciaturas', 'null');
    }
    dipl(v) { //SE USA SE USA SE USA SE USA SE USA SE USA SE USA SE USA SE USA SE USA SE USA
        $('body').addClass('white').removeClass('grey');
        $('.a').removeClass('scale-in');
        $('.b').removeClass('scale-in');
        setTimeout(() => {
            $('.a').addClass('scale-in');
            setTimeout(() => {
                $('.b').addClass('scale-in');
            }, 201);
        }, 201);
        $('.parallax1').parallax();
        $('.carousel').carousel({
            dist: 0,
            numVisible: 3
        });
        $("html, body").stop().animate({ scrollTop: 0 }, 500, 'swing');
        $.cookie('posgrado', 'null')
        $.cookie('licenciaturas', 'null')
        $('.tabs').tabs();
    }
    us(v) {
        $('body').addClass('grey').removeClass('white');
        $('.tabs').tabs('select', 'test5');
    }
    student_services() {
        $('.tabs').tabs('select', 'test3');
    }
}