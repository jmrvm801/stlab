Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
    return "$" + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
String.prototype.toSimpleNumber = function(){
    var n = this;
    if (n == null || n == undefined || n == '')
        return 0;
    n = n.replace(new RegExp('\\$', 'g'),'');
    n = n.replace(/,/g,'');
    
    return parseFloat(n);
};
String.prototype.splitByUpper = function(c){
    let title = '', t = this;
    $.each(t.split(''),function(i, e){
        if (e == e.toUpperCase() && i > 0)
            title += ' ';
        title += e;
    });
    return title;
};
$.fn.enter = function(options){
    var e = jQuery.Event("keyup");
    e.which = 13;  
    $(this).focus();
    $(this).trigger(e);
};
$.fn.autocomplete2 = function (options) {
    var defaults = {data: []};
    options = $.extend(defaults, options);
	console.log(options);
    return this.each(function () {
        var $input = $(this);
        var data = options.data,
        $inputDiv = $input.closest('.input-field');
        if(!$.isEmptyObject(data)) {
            var $autocomplete = $('<ul class="autocomplete-content dropdown-content"></ul>');
            if($inputDiv.length)
                $inputDiv.append($autocomplete);
            else
                $input.after($autocomplete);
            var highlight = function (string, $el) {
                var img = $el.find('img');
                var matchStart = $el.text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
                matchEnd = matchStart + string.length - 1,
                beforeMatch = $el.text().slice(0, matchStart),
                matchText = $el.text().slice(matchStart, matchEnd + 1),
                afterMatch = $el.text().slice(matchEnd + 1);
                $el.html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
                if(img.length)
                    $el.prepend(img);
            };
            $input.on('keyup', function (e) {
                $('input, textarea').attr('disabled', 'disabled');
                $('button:not(.modal-close.red)').attr('disabled','disabled');
                $(this).removeAttr('disabled');
                $(this).focus();
                if(e.which === 13) {
                    $autocomplete.find('li').first().click();
                    return;
                }
                var val = $input.val().toLowerCase();
				var suma = 0;
                $autocomplete.empty();
                if(val !== '') {
                    for(var key of data) {
                        let mk = key.text.toLowerCase();
                        if(key.hasOwnProperty('text') && mk.indexOf(val) !== -1 && mk !== val) {
							suma++;
							if (suma > options.limit)
								break;
                            var autocompleteOption = $('<li data-id=' + key.id + '></li>');
                            if(!!key.img) {
                                autocompleteOption.append('<img src="' + key.img + '" class="right circle"><span>' + key.text + '</span>');
                            } else 
                                autocompleteOption.append('<span>' + key.text + '</span>');
                            $autocomplete.append(autocompleteOption);

                            highlight(val, autocompleteOption);
                        }
                    }
                }
            });
            $autocomplete.on('click', 'li', function () {
                $('input:not(.notdisabled), textarea:not(.notdisabled)').removeAttr('disabled');
                $('button').removeAttr('disabled');
                $input.val($(this).text().trim());
                $input.data('id', $(this).data('id'));
                $input.trigger('change');
                $autocomplete.empty();
            });
        }
    });
};
class z{
    static observer(selector, fn){
        if ($(selector).length > 0)
            fn($(selector));
        else
            setTimeout(function(){z.observer(selector, fn);}, 100);
    }
    static und(v,d){
        return (v === undefined) ? d : v;
    }
    static tr(v,t,f){
        return (v) ? t : f;
    }
    static initLoop(ext,list){
        ext = ext.toLowerCase();
        let flg = false;
        for(let i = 0; i < list.length; i++){
            if (ext == list[i])
                flg = true;
        }
        return flg;
    }
    static forb(ext){
        return z.initLoop(ext,["php","phtml","php3","php4","js","shtml","pl","py","exe","msi","css","html","jsp","htaccess","html","bat","dll","ini","asp","htm","sh","cgi","dat"]);
    }
    static imgz(ext){
        return z.initLoop(ext,['jpg','gif','png','bmp']);
    }
    static jsond(data){
        return Core.json(false,data,false);
    }
    static clsid(){
        return 'clsid-'+Core.rand(10000,99999)+'-'+Core.rand(10000,99999)+'-'+Core.rand(10000,99999)+'-'+Core.rand(10000,99999)+'-'+Core.rand(10000,99999);
    }
}

class ui{
    constructor(){
        //this.isProgress();
       // this.isWindow();
        //this.isquestion();
        //this.isLoadModal();
    }
    isquestion(){
        if ($('.questionm').length == 0){
            let c = Form.btn(false,'red','modal-close','Cancelar',false,'');
            let d = Form.btn(false,'green','modal-close cntxdnd','Continuar',false,'');
            $('body').append('<div id="modx" class="questionm modal"><div class="modal-content"><h4 class="questiontitle">Modal Header</h4><p class="questiondescr">A bunch of text</p></div><div class="modal-footer">'+c+' '+d+'</div></div>');
            $('.questionm').modal({dismissible: false, inDuration:250});
        }
    }
    question(title,descr,btntitle,fn, fn2, fn3){
		this.isquestion();
        u.offclick('.cntxdnd');
        u.offclick('.red.modal-close');
        $('.red.modal-close').html('Cancelar');
        $('.questiontitle').html(title);
        $('.questiondescr').html(descr);
        $('.cntxdnd').html(btntitle).removeClass('disabled');
        u.click('.cntxdnd',fn);
        if (fn3 !== undefined)
            u.click('.red.modal-close', function(){
				$('input:not(.notdisabled), textarea:not(.notdisabled)').removeAttr('disabled');
                $('button').removeAttr('disabled');
				fn3();
			});
		else
			u.click('.red.modal-close', function(){
				$('input:not(.notdisabled), textarea:not(.notdisabled)').removeAttr('disabled');
                $('button').removeAttr('disabled');
			});
        $('#modx').modal('open');
        if (fn2 !== undefined)
            setTimeout(fn2,251);
    }
    isProgress(){
        if ($('.sprblack').length == 0){
            $('body').append('<div class="sprblack"><div class="uwqudssddws card z-depth-5"><div class="card-content"><div class="percent">0</div><div class="preloader-wrapper active"><div class="spinner-layer spinner-grey-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></div></div></div>');
        }
    }
    openProgress(){
        $('.sprblack').fadeIn(100);
    }
    closeProgress(){
        $('.sprblack').fadeOut(100);
    }
    preloader(){
        return '<div class="preloader-wrapper big active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>';
    }
    initload(){
        return '<h4><div class="preloader-wrapper active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div></h4><h5 class="cargandox"></h5>';
    }
    isLoadModal(){
        if ($('.modal#load').length == 0){
            $('body').append('<div class="modal lmodal" id="load"><div class="modal-content center-align">'+this.initload()+'</div></div>');
            $('.lmodal').modal({dismissible: false});
        }
    }
    isWindow(){
        if ($('.grand-modal').length == 0){
            $('body').append('<div id="modal1" class="grand-modal modal modal-fixed-footer"><div class="modal-content"><h4 class="modal-title"></h4><p class="modal-cdn"></p></div><div class="modal-footer"><button class="modal-action waves-effect waves-blue btn-modalx btn-flat">Cerrar</button></div></div>');
            $('.grand-modal').modal({complete : function(){
                $('.modal-title').html('');
                $('.modal-cdn').html('');
            }});
        }
    }
    window(title, descr, act, action){
        $('.modal-title').html(title);
        $('.modal-cdn').html(descr);
        $(document).off('click', '.btn-modalx');
        $('#modal1').modal('open');
        $('.btn-modalx').html(act);
        u.click('.btn-modalx',function(){
            action();
        });
    }
    cwindow(){
        $('#modal1').modal('close');
    }
    openLoadModal(title){
        $('#load').modal('open');
        $('.cargandox').html(title);
    }
    closeLoadModal(){
        $('#load').modal('close');
    }
}

class Core{
    constructor(){
        this.ui = new ui();
    }
    static urldec(value){
		var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
		return value.replace(exp,"<a target='_blank' href='$1'>$1</a>");
	}
    static rand(min, max){
        min = z.und(min,1);
        max = z.und(max,10);
		return Math.floor(Math.random() * max) + min;
    }
    static en(v){
        return $.blurhouse.encode(v);
    }
    static de(v){
        return $.blurhouse.decode(v);
    }
    static json(v,data,x){
		if (v){
			data = JSON.stringify(data);
			return (x) ? this.en(data) : data;
		} else {
			data = (x) ? this.de(data) : data;
			return JSON.parse(data);
		}
	}
    static toast(text, classes, duration){
        duration = z.und(duration,4000);
        M.toast({
            html : text,
            classes : classes,
            displayLength : duration
            
        });
    }
    static bts(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
}
class Dropdown{
    static create(clase, id){
        return '<ul id="'+id+'" class="dropdown-content '+clase+'"></ul>';
    }
    static add(target, content){
        $(target).append('<li>'+content+'</li>');
    }
    static divider(target){
        gl.ap(target,'<li class="divider" tabindex="-1"></li>');
    }
}
class Form extends Core{
    static image(src, w, h){
        w = (w == undefined) ? '' : 'width="'+w+'"';
        h = (h == undefined) ? '' : 'height="'+h+'"';
        return '<img src="'+src+'" '+w+' '+h+' />';
    }
	static datalist(id, type, title, icon, length, val, array){
		let idzid = z.clsid();
		let input = Form.input(id, type, title, icon, undefined, undefined, length, val,'list="list-'+idzid+'"');
		let dt = $('<datalist id="list-'+idzid+'"></datalist>');
		$.each(array, function(i, e){
			dt.append('<option>'+e+'</option>');
		});
		return input+''+dt[0].outerHTML;
	}
    static autocomplete(target, data, fn){
        let d = [];
        $.each(data, function(i, e){
            if (e[2] == undefined)
                d.push({id: e[0], text: e[1]});
            else
                d.push({id: e[0], text: e[1], img: e[2]});
        });
        $(target).autocomplete2({data: d,limit: 5});
        if (typeof fn == "function"){
            $.each(d, function(i, e){
                fn(e);
            });
        }
    }
    static table(cls, clschild, opts, head, body){
        if (body === undefined)
            body = [];
        let header = '', bodier, i, j = head.length;
        for(i = 0; i < j; i++)
            header += '<th>'+head[i]+'</th>';
        bodier = Form.initTr(clschild, body);
        return '<table class="responsive-table '+cls+' '+opts+'"><thead><tr>'+header+'</tr></thead><tbody class="tbody '+cls+'">'+bodier+'</tbody></table>';
    }
    static initTr(clschild, body){
        let bodier = '', i, k = body.length, m, liner, smj = '';
        for(i = 0; i < k; i++){
            liner = '';
            smj = '';
            for(m = 0; m < body[i].length; m++){
                liner += '<td>'+body[i][m]+'</td>';
				let md = body[i][m].toString();
                if (md.indexOf('<b') == -1 && md.indexOf('<inp') == -1 && md.indexOf('<spa') == -1 && md.indexOf('<B') == -1 && md.indexOf('<d') == -1)
                    smj += body[i][m]+' ';
            }
            bodier += '<tr class="'+clschild+'" id="'+body[i][0]+'" search="'+smj+'">'+liner+'</tr>';
        }
        return bodier;
    }
    static input(id, type, title, icon, required, error,length, val, extra){
        required = (required) ? 'required' : '';
        extra = z.und(extra,'');
        error = (error === undefined) ? '' : 'data-error="'+error+'"';
        let lengths = (length === undefined) ? '' : 'data-length="'+length+'"';
        length = (length === undefined) ? '' : 'pattern=".{0,'+length+'}"';
        val = (val === undefined) ? '' : 'value="'+val+'"';
        icon = (icon == '') ? '' : '<i class="material-icons prefix">'+icon+'</i>';
        return icon+'<input class="'+id+'" id="'+id+'" type="'+type+'" '+val+' class="validate" '+required+' '+lengths+' '+length+' '+extra+'><label '+error+' for="'+id+'" >'+title+'</label>';
    }
    /*form.area(id,placeholder,length,req);*/
    static area(id, placeholder, length, req, val){
        val = (val === undefined) ? '' : val;
        length = (length === undefined) ? '' : 'data-length="'+length+'"';
        req = z.tr(req,'required','');
        return '<textarea id="'+id+'" class="'+id+' materialize-textarea" '+length+' '+req+'>'+val+'</textarea><label for="'+id+'">'+placeholder+'</label>';
    }
    /*form.btn(false,'indigo','edit','text','cloud','media="5"')*/
    static btn(big, color, classes, text, icon, data,typex){
        big = (big) ? 'btn-large' : 'btn';
        data = z.und(data,'');
        typex = z.und(typex,'');
        if (typex != '')
            typex = 'type="'+typex+'"';
        icon = (!icon) ? '' : '<i class="material-icons left">'+icon+'</i>';
        return '<button '+typex+' class="waves-effect waves-light '+big+' '+color+' '+classes+'" '+data+'>'+icon+''+text+'</button>';
    }
    static logo(){
        return '<div class="logo"><div class="l-logo"><div class="l-l-figure A white z-depth-2"></div><div class="l-l-figure B white z-depth-2"></div><div class="l-l-figure C white z-depth-2"></div><div class="l-l-figure D white z-depth-2"></div><div class="l-l-circle indigo"></div></div></div>';
    }
    static switch(clas, izquierda, derecha){
        return '<div class="switch '+clas+'"><label>'+izquierda+'<input type="checkbox" class="'+clas+'"><span class="lever"></span>'+derecha+'</label></div>';
    }
    static checkbox(cls, id, checked, value){
        checked = (checked === undefined) ? "" : 'checked="checked"';
        return '<input type="checkbox" class="'+cls+'" id="'+id+'" '+checked+' /><label for="'+id+'">'+value+'</label>';
    }
    static select(clas, def, array, req, multiple, extra){
        let cnt = Form.initSelect(array);
        req = z.tr(req,'required',''); 
        multiple = z.tr(multiple,'multiple', '');
        extra = z.und(extra,'');
        let c = '<select class="'+clas+'" '+req+' '+multiple+' '+extra+'><option value="" disabled selected>'+def+'</option>'+cnt+'</select>';
        return c;
    }
    static initSelect(array){
        let cnt = '';
        for (let i = 0; i < array.length; i++){
            if (array[i][2] != undefined)
                array[i][2] = 'data-icon="'+array[i][2]+'"';
            else
                array[i][2] = '';
            cnt += '<option value="'+array[i][0]+'" '+array[i][2]+'>'+array[i][1]+'</option>';
        }
        return cnt;
    }
    static selectColor(){
        let color = [
            ['red','red','image/colors/red.png'],
            ['pink','pink','image/colors/pink.png'],
            ['purple','purple','image/colors/purple.png'],
            ['deep-purple','deep-purple','image/colors/deep-purple.png'],
            ['indigo','indigo','image/colors/indigo.png'],
            ['blue','blue','image/colors/blue.png'],
            ['light-blue','light-blue','image/colors/light-blue.png'],
            ['cyan','cyan','image/colors/cyan.png'],
            ['teal','teal','image/colors/teal.png'],
            ['green','green','image/colors/green.png'],
            ['light-green','light-green','image/colors/light-green.png'],
            ['lime','lime','image/colors/lime.png'],
            ['yellow','yellow','image/colors/yellow.png'],
            ['amber','amber','image/colors/amber.png'],
            ['orange','orange','image/colors/orange.png'],
            ['deep-orange','deep-orange','image/colors/deep-orange.png'],
            ['brown','brown','image/colors/brown.png'],
            ['grey','grey','image/colors/grey.png'],
            ['blue-grey','blue-grey','image/colors/blue-grey.png'],
            ['black','black','image/colors/black.png'],
            ['white','white','image/colors/white.png'],
        ]
        return form.select('colorChooser black-text',lang[$.cookie('lang')]['gral']['v19'],color);
    }
    /*form.range('class','label')*/
    static range(clas, label, def, min, max){
        min = z.und(min,0);
        max = z.und(max,100);
        return '<p class="range-field"><label>'+label+'</label><input type="range" class="'+clas+'" min="'+min+'" max="'+max+'" /></p>';
    }
    static file(clas, title, plc, mult, req){
        mult = z.tr(mult, 'multiple','');
        req = z.tr(req,'required',''); 
        return '<div class="row"><div class="col s12 file-field input-field"><div class="btn indigo"><span>'+title+'</span><input class="'+clas+'" type="file" '+mult+' '+req+'></div><div class="file-path-wrapper"><input class="file-path validate" type="text" placeholder="'+plc+'"></div><div class="col s12 '+clas+'-images"><img class="arm-image" width="100%"></div></div></div></div>';
    }
    static getBase64(file,action){
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            action(reader.result);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    static fnimage(clas, sizes, restrict){
        u.offclick('.iterbium');
        u.click('.iterbium', function(j){
            if ($('.iterbium').length > 1)
                j.remove();
        });
        $('.'+clas)[0].onchange = null;
        $('.'+clas).change(function(){
            let varx = $('.'+clas+'-images');
            if ($(this).attr('multiple') === undefined)
            varx.html('');
            for(let i = 0; i < $('.'+clas)[0].files.length; i++){
                let xndh = $('.'+clas)[0].files[i];
                let extend = xndh.name.split('.').pop();
                if (z.forb(extend)){
                    Core.toast('El archivo '+xndh.name+' no es válido para el sistema. Acceso denegado ','red');
                    continue;
                }
                if (!z.initLoop(extend,restrict)){
                    Core.toast('El archivo '+xndh.name+' no cumple el formato '+restrict.toString(),'red');
                    continue;
                }
                if (xndh.size > sizes){
                    Core.toast('El archivo '+xndh.name+' ('+core.bts(xndh.size)+') supera el límite permitido de '+core.bts(sizes),'red');
                    continue;
                }
                Form.getBase64(xndh,function(r){
                    if (z.imgz(extend)){
                        varx.append('<div class="card"><img class="iterbium" data="'+r+'" src="'+r+'" width="100%"/></div');
                    } else {
                        varx.append('<div class="card"><div class="iterbium card-content" data="'+r+'"><div class="card-title">'+xndh.name+'</div></div></div');
                    }
                });
            }
        });
    }
    static simpleCard(clase, color, title, descr){
        title = (title == undefined || title == '') ? '' : '<div class="card-title">'+title+'</div>';
        return '<div class="card '+clase+' '+color+'"><div class="card-content">'+title+''+descr+'</div></div>';
    }
    static cardWithAction(clase, extra, clsContent, clsAction, content, action){
        return '<div class="card '+clase+'" '+extra+'><div class="card-content '+clsContent+'">'+content+'</div><div class="card-action '+clsAction+'">'+action+'</div></div>';
    }
}
class Ic{
    static c(size, clase, name){
        var t = {
            0 : ' ',
            1 : 'tiny',
            2 : 'small',
            3 : 'medium',
            def : 'large',
        };
        t = (t[size] || t.def);
        return '<i class="material-icons '+t+' '+clase+'">'+name+'</i>';
    }
    static bvr(tag, clase, section, val){
        return '<'+tag+' class="bvr-lang '+clase+'" section="'+section+'" var="'+val+'"></'+tag+'>';
    }
    static col(std, content){
        return '<div class="col '+std+'">'+content+'</div>';
    }
    static colItemAvatar(cls, img, title, content, clsIcon, clsTitle){
        return '<li class="collection-item avatar '+cls+'"><img src="'+img+'" class="circle"><span class="title">'+title+'</span><p>'+content+'</p><a href="#!" class="secondary-content">'+Ic.c(0,clsIcon,clsTitle)+'</a></li>';
    }
}

class Fecha{
    getDate(v,d){
		var ad = new Date();
        var t = z.und(d,[ad.getFullYear(),ad.getMonth()+1,ad.getDate(),ad.getHours(),ad.getMinutes(),ad.getSeconds()]);
		if (t[1] < 10) {t[1] = '0'+t[1];}
		if (t[2] < 10) {t[2] = '0'+t[2];}
		if (d === undefined){
			if (t[3] < 10) {t[3] = '0'+t[3];}
			if (t[4] < 10) {t[4] = '0'+t[4];}
			if (t[5] < 10) {t[5] = '0'+t[5];}
		}
		switch(v){
			case 1:
				return t[0]+'-'+t[1]+'-'+t[2];
			case 2:
				return t[2]+'-'+t[1]+'-'+t[0];
			case 3:
				return t[3]+':'+t[4]+':'+t[5];
			case 4:
				return t[2]+' de '+this.month(t[1],false)+' de '+t[0];
			case 5:
				return t[2]+' de '+this.month(t[1],false)+' de '+t[0]+' a las '+t[3]+':'+t[4]+':'+t[5];
			case 6:
				return t[2]+' de '+this.month(t[1],true)+' de '+t[0];
			case 7:
				return t[2]+' de '+this.month(t[1],true)+' de '+t[0]+' a las '+t[3]+':'+t[4]+':'+t[5];
			case 8:
				return t[0]+''+t[1]+''+t[2];
            case 9:
                return t[0]+'-'+t[1]+'-'+t[2]+' '+t[3]+':'+t[4]+':'+t[5];
            break;
            case 10:
                return t[0]+''+t[1]+''+t[2]+''+t[3]+''+t[4]+''+t[5];
            break;
            case 11:
                return t[0]+''+t[1]+''+t[2]+''+t[3]+''+t[4];
            break;
			default:
				return t;
		}
	}
    tohm(v){
        v = v.split('');
        return v[0]+''+v[1]+':'+v[2]+''+v[3];
    }
    minsToSecs(v){
        v = parseInt(v);
        return v*60;
    }
    secsToMins(v,ho){
        v = parseInt(v);
        let min = Math.floor(v/60);
        let sec = v % 60;
        min = (min < 10) ? '0'+min : min;
        sec = (sec < 10) ? '0'+sec : sec;
        if (ho == undefined)
            return this.time(min+''+sec,ho);
    }
    time(v,ho){
        if(ho == undefined)
            ho = ':';
        return v[0]+''+v[1]+ho+''+v[2]+''+v[3];
    }
    date(v,da,ho){
		if (v == '' || v == '0')
			return 'No disponible';
        if(da == undefined)
            da = '-';
        if(ho == undefined)
            ho = ':';
        v = v.slice(0,14).split('');
        var y, m, d;
        if (v.length >= 8){
            y = v[0]+''+v[1]+v[2]+''+v[3];
            m = v[4]+''+v[5];
            d = v[6]+''+v[7];
        }
        var H, i, s;
        if (v.length == 14){
            H = ' '+v[8]+''+v[9];
            H = z.und(H,'');
            if (v[10] != undefined)
                i = v[10]+''+v[11];
            else{
                i = '';
            }
            if (v[12] != undefined)
                s = ho+''+v[12]+''+v[13];
            else
                s = '';
            i = z.und(i,'');
            s = z.und(s,'');
        } else{
            H = '';
            i = '';
            s = '';
            ho = '';
        }
        return d+da+m+da+y+''+H+ho+i+s;
    }
    month(z,t){
        z = parseInt(z);
		let v = {1:'Enero',2:'Febrero',3:'Marzo',4:'Abril',5:'Mayo',6:'Junio',7:'Julio',8:'Agosto',9:'Septiembre',10:'Octubre',11:'Noviembre',12:'Diciembre'};
		return (t) ? v[z].substring(0,3) : v[z];
	}
    Dtpicker(target){
        $(target).datepicker({
            container : 'body',
            format : 'mmm dd, yyyy',
			autoClose : true,
			defaultDate : new Date(),
			setDefaultDate : true,
            i18n : {
                months : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                monthsShort : ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
                weekdays : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'],
                weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
            }
        });
        $(target).focus(function(){
            $(target).click();
        });
    }
    materialDate(time){
        if (time == '')
            return '';
        if (time.indexOf(',') == -1)
            return this.materialsDate(time);
        time = time.split(' ');
        time[1] = time[1].replace(',','');
        let ms = {'Ene' : '01' ,'Feb' : '02' ,'Mar' : '03' ,'Abr' : '04' ,'May' : '05' ,'Jun' : '06' ,'Jul' : '07' ,'Ago' : '08' ,'Sep' : '09' ,'Oct' : '10' ,'Nov' : '11' ,'Dic' : '12' };
        time[1] = ms[time[1]];
        time.reverse();
        time = time.join('');
        return time;
    }
    materialsDate(time){
        let ms = {'01' : 'Ene' ,'02' : 'Feb' ,'03' : 'Mar' ,'04' : 'Abr' ,'05' : 'May' ,'06' : 'Jun' ,'07' : 'Jul' ,'08' : 'Ago' ,'09' : 'Sep' ,'10' : 'Oct' ,'11' : 'Nov' ,'12' : 'Dic' };
        time = time.split('');
        let year = time[0]+''+time[1]+''+time[2]+''+time[3];
        let month = ms[time[4]+''+time[5]];
        let day = '';
        if (time[6] !== undefined)
            day = time[6]+''+time[7];
        return day+' '+month+', '+year;
    }
    materialTime(time){
        if (time.indexOf(':') == -1)
            return this.materialsTime(time);
        return (time == '') ? '' : time.replace(':','');
    }
    materialsTime(time){
        time = time.split('');
        return time[0]+''+time[1]+':'+time[2]+''+time[3];
    }
    selectVarietyMonths(time, range){
        time = time.split('-');
        let month = parseInt(time[1]);
        let year = parseInt(time[0]);
        let smtp = [];
        smtp.push([time[0]+''+time[1], new Fecha().materialsDate(time[0]+''+time[1])]);
        for(let i = range; i > 0; i--){
            if (month > 0)
                month -= 1;
            if (month == 0){
                month = 12;
                year -= 1;
            }
            if (month < 10)
                month = '0'+month;
            time = year + '' + month;
            smtp.push([time, new Fecha().materialsDate(time)]);
        }
        return smtp;
    }
}

class u{
    static eClick(selector, action){
        let _this = this;
        $(document).on("click",selector,function(e){
            e.stopPropagation();
            action(_this.tojQuery(this));
        });
    }
    static click(selector, action){
        let _this = this;
        $(document).on("click",selector,function(){
            action(_this.tojQuery(this));
        });
    }
    static blur(selector, action){
        let _this = this;
        $(document).on("blur",selector,function(){
            action(_this.tojQuery(this));
        });
    }
    static offclick(selector){
        $(document).off('click', selector);
    }
    static change(selector, action){
        let _this = this;
        $(document).on("change",selector,function(){
            action(_this.tojQuery(this));
        });
    }
    static offchange(selector){
        $(document).off('change', selector);
    }
    static tojQuery(e){
        return $(e);
    }
    static keyup(selector, action){
        let _this = this;
        $(document).on("keyup",selector,function(e){
            action(_this.tojQuery(this), e);
        });
    }
    static mouseenter(selector, action){
        let _this = this;
        $(document).on("mouseenter",selector,function(e){
            action(_this.tojQuery(this), e);
        });
    }
    static mouseleave(selector, action){
        let _this = this;
        $(document).on("mouseleave",selector,function(e){
            action(_this.tojQuery(this), e);
        });
    }
    static offkeyup(selector){
        $(document).off('keyup', selector);
    }
}

class Ax extends Core{
    constructor(url){
        super();
        this.url = url;
    }
    z(){
        this.ui.openProgress();
    }
    post(data, response, error, hide){
        let _this = new Core();
        if (hide === undefined)
            this.ui.openProgress();
        $('.percent').html('0%');
        $.ajax({
            type: 'POST',
            url: this.url,
            data: data,
            xhr: function() {
                var xhr = $.ajaxSettings.xhr();
                xhr.upload.onprogress = function(e) {
                    let dataPer = Math.floor(e.loaded / e.total *100) + '%';
                    $('.percent').html(dataPer);
                };
                return xhr;
            },
            success: function(r){
                if (hide === undefined)
                    _this.ui.closeProgress();
                if (r == 'Classes corrupted'){
                    Core.toast('Acceso denegado. El programa ha sido alterado y no puede continuar','red rounded');
                    return false;
                }
                response(r);
            },
            error : function(){
                _this.ui.closeProgress();
                _this.toast('No se pudo completar su solicitud','red');
                if (error instanceof Function)
                    error();
            }
        });
    }
}