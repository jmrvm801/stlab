<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="theme-color" content="#e9ab56">
    <title>Laboratorios Starlab</title>
    <link rel="icon" href="assets/img/favicon.ico">
    <!--<script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(75972)</script>-->
    <!-- Libs scripts -->
    <script type="text/javascript" src="assets/libs/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="assets/libs/jquery-ui.js"></script>
    <script type="text/javascript" src="assets/libs/blurhouse.js"></script>
    <script type="text/javascript" src="assets/libs/cookie.js"></script>
    <script type="text/javascript" src="assets/libs/materialize.js"></script>
    <script type="text/javascript" src="assets/libs/util.js"></script>
    <!-- Business scripts -->
    <script type="text/javascript" src="assets/js/index.js?random=<?php echo filemtime('assets/js/index.js'); ?>"></script>
    <!-- CSS scripts -->
    <link type="text/css" rel="stylesheet" href="assets/css/core.css">
    <link type="text/css" rel="stylesheet" href="assets/css/icon.css" />
    <link type="text/css" rel="stylesheet" href="assets/css/materialize.css" media="screen,projection,print" />
    <link type="text/css" rel="stylesheet" href="assets/css/index.css?random=<?php echo filemtime('assets/css/index.css'); ?>">
</head>

<body class="app grey lighten-4">
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_LA/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="110836697506918"
  theme_color="#406090"
  logged_in_greeting="Bienvenido a Starlab laboratorios. ¿En qué podemos servirle?"
  logged_out_greeting="Bienvenido a Starlab laboratorios. ¿En qué podemos servirle?">
      </div>
    <noscript>
        <div class="containerf">
            <div class="card mwindow">
                <div class="card-content">
                    <p>Se necesita javascript para ejecutar este sitio correctamente</p>
                </div>
                <div class="card-action">
                    <a href="#" class="z-depth-0 btn white materialize-red-text waves-effect">Activar javascript</a>
                </div>
            </div>
        </div>
    </noscript>
    <div class="loading">
        <div class="l-window center-align">
            <img src="assets/img/logo_horizontal.png" width="220" />
            <div class="sptower progress">
                <div class="indeterminate"></div>
            </div>
        </div>
    </div>
    <div class="app">
        <ul id="licenciaturas" class="dropdown-content">
            <li><a class="oedu idea_color polarizado" data-target="progresivos_s" href="#!">Progresivos</a></li>
            <li><a class="oedu idea_color polarizado" data-target="polarizados_s" href="#!">Polarizados</a></li>
            <li><a class="oedu idea_color polarizado" data-target="fotosensibles_s" href="#!">Fotosensibles</a></li>
            <li><a class="oedu idea_color polarizado" data-target="antireflejantes_s" href="#!">Antireflejantes</a></li>
            <li><a class="oedu idea_color polarizado" data-target="contacto_s" href="#!">Lentes de contacto</a></li>
        </ul>

        <div class="navbar-fixed">
            <nav class="nav-extended transparent">
                <div class="nav-wrapper white">
                    <a href="#" class="brand-logo"><img src="assets/img/logo_horizontal.png" height="50" /></a>
                    <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons black-text">menu</i></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a class="black-text" href="tel:800 712 0150">800 712 0150<i class="material-icons left idea_color">phone</i></a></li>
                        <li><a class="black-text" href="https://silo.starlab.com.mx" target="_blank">Pedidos<i class="material-icons left idea_color">local_mall</i></a></li>
                    </ul>
                </div>
                <div class="nav-content idea_back">
                    <ul class="tabs tabs-transparent">
                        <li class="tab">
                            <a class="home active" href="#test1">Inicio</a>
                        </li>
                        <li class="tab">
                            <a class="dipl" href="#test8">Marca Refract</a>
                        </li>
                        <li class="tab">
                            <a class="oedu dropdown-trigger" href="#test4" data-target="licenciaturas">Productos</a>
                        </li>
                        <li class="tab">
                            <a class="services" href="#test3">Laboratorio</a>
                        </li>
                        <li class="tab">
                            <a class="contacto" href="#test6">Contacto</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <ul class="sidenav" id="mobile-demo">
            <li>
                <div class="user-view">
                    <div class="background grey lighten-3">
                        <!-- <img src="assets/img/sitiosweb.jpg"> -->
                    </div>
                    <a href="#user"><img class="circle" src="assets/img/fav_png.png"></a>
                    <a href="#name"><span class="black-text name">IDEA de Oriente</span></a>
                    <a href="#email"><span class="black-text email">Tú universidad... tu mejor idea</span></a>
                </div>
            </li>
            <li><a href="https://idea.grupomarssoft.com">Sistema Escolar IDEA</a></li>
            <li><a href="tel:2727250007">Llamada rápida</a></li>
        </ul>

        <div id="test1" class="col s12">
            <div class="slider">
                <ul class="slides">
                    <li class="grey darken-4">
                        <img src="assets/img/sliders/001.png">
                    </li>
                    <li class="grey darken-4">
                        <img src="assets/img/sliders/002.png">
                    </li>
                    <li class="grey darken-4">
                        <img src="assets/img/sliders/003.png">
                    </li>

                </ul>
            </div>
            <div class="container">
                <div class="row">
                    <div class="c col s12 scale-transition scale-out">
                        <h4 class="center-align">20 años de <span class="idea_color">experiencia</span> nos respaldan</h4>
                    </div>

                    <div class="d col s12 m4 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <img src="assets/img/001.jpg">
                                <span class="card-title">Tallado digital</span>
                            </div>
                            <div class="card-content">
                                <p>Nuestros equipos con tecnología de punta procesan tu lente con una exactitud milimétrica.</p>
                            </div>
                            <div class="card-action">
                                <a class="idea_color" href="#">Saber más</a>
                            </div>
                        </div>
                    </div>

                    <div class="d col s12 m4 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <img src="assets/img/001.jpg">
                                <span class="card-title">Recubrimientos</span>
                            </div>
                            <div class="card-content">
                                <p>Utilizamos Antireflejantes y antirayas de primer nivel para unos lentes duraderos.</p>
                            </div>
                            <div class="card-action">
                                <a class="idea_color" href="#">Saber más</a>
                            </div>
                        </div>
                    </div>

                    <div class="d col s12 m4 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <img src="assets/img/001.jpg">
                                <span class="card-title">Matariales</span>
                            </div>
                            <div class="card-content">
                                <p>Trabajamos con los materiales más utilizados del mercado para brindarte satisfacción.</p>
                            </div>
                            <div class="card-action">
                                <a class="idea_color" href="#">Saber más</a>
                            </div>
                        </div>
                    </div> <!--  d e -->

                    <div class="d col s12 scale-transition scale-out">
                        <h4 class="center-align">Presentamos nuestra <span class="idea_color">nueva</span> marca</h4>
                    </div>

                    <div class="d col s12 scale-transition scale-out">
                        <div class="card-panel center-align">
                            <img src="assets/img/refract_logo.png" width="300">
                            <p class="justify-align first_paragraph">Las lentes Refract diseñadas por Starlab Laboratorios Ópticos incorporan la tecnología más sofisticada del mercado en la creación de lentes digitales <span class="idea_color">FreeForm</span>.</p>
                            <div class="card-action">
                                <a class="idea_color" href="#">Quiero saber más</a>
                            </div>
                            <div class="card-back">
                                <img src="assets/img/left_color.png" width="300">
                            </div>
                        </div>
                    </div>

                    <div class="e col s12 m4 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <iframe width="100%" height="350" src="https://www.youtube.com/embed/uRys5-daDbI" frameborder="0" allowfullscreen=""></iframe>
                                
                            </div>
                            <div class="card-content">
                                <span class="card-title">Essilor</span>
                                <p class="justify-align">Essilor es el líder mundial en soluciones para la visión. Presente en más de 100 países, el éxito del Grupo es el resultado de una estrategia que ha sido impulsada por la innovación desde hace más de 160 años. Desde el diseño a la fabricación, el grupo desarrolla una amplia gama de lentes para corregir la vista.</p>
                            </div>
                        </div>
                    </div>

                    <div class="e col s12 m4 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <iframe width="100%" height="350" src="https://www.youtube.com/embed/RFuoIuxwA-0" frameborder="0" allowfullscreen=""></iframe>
                                
                            </div>
                            <div class="card-content">
                                <span class="card-title">Transitions</span>
                                <p class="justify-align">Gracias a una innovación constante, los productos Transitions mejoran la visión ajustándose y adaptándose a la luz cambiante. Mediante nuestras asociaciones con los líderes del sector óptico, ofrecemos lentes dinámicas con la mayor selección de diseños y materiales, lo que incluye cientos de combinaciones de lentes.</p>
                            </div>
                        </div>
                    </div>

                    <div class="e col s12 m4 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <iframe width="100%" height="350" src="https://www.youtube.com/embed/TizLH70Ry7k" frameborder="0" allowfullscreen=""></iframe>
                                
                            </div>
                            <div class="card-content">
                                <span class="card-title">Zeiss</span>
                                <p class="justify-align">ZEISS ha contribuido durante más de 160 años al progreso tecnológico mundial de mercados como las soluciones industriales, las soluciones de investigación, la tecnología médica y la óptica de consumo, mejorando así la calidad de vida de muchas personas.</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div id="test8" class="col s12">
            <div class="col s12">
                <div class="parallax1 parallax-container">
                    <div class="parallax parallax1">
                        <img src="assets/img/color_refract_blur.png">
                    </div>
                    <div class="logo_refract_centering center-align">
                        <img src="assets/img/refract_logo.png" width="300px">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="content_b">
                        <div class="a col s12 scale-transition scale-out">
                            <h4>Tecnologías de vanguardia</h4>
                            <p class="text-data-b">
                            El laboratorio independiente Starlab incorpora la tecnología más sofisticada del mercado en la creación de lentes digitales <span class="idea_color">FreeForm</span>.
                            </p>
                        </div>

                        <div class="col s12 m4 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/digital_ray_path.png">                                    
                                </div>
                                <div class="card-content">
                                    <img src="assets/img/digital_ray_path_name.png" width="100%">
                                    <p class="justify-align content_beauty">
                                    La tecnología más avanzada para producir lentes digitales. La cara interna del lente se calcula mediante un método tridimensional que tiene en cuenta la posición real del lente y los movimientos naturales que realiza el ojo humano.<br><br>
                                    El resultado de este cálculo tan preciso es un lente totalmente personalizado que ofrece una excelente visión en todas las zonas del lente.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m4 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/surface_power.png">                                    
                                </div>
                                <div class="card-content">
                                    <img src="assets/img/surface_power_name.png" width="100%">
                                    <p class="justify-align content_beauty">
                                    Es la tecnología de entrada de gama para fabricar lentes digitales. Los lentes producidos mediante esta tecnología cuentan con el tallado del progresivo en la cara interna del lente y una curva simple, que generalmente es una esfera, en la cara externa del mismo.<br><br>
                                    El resultado es un lente con calidad visual similar a un progresivo convencional pero con los beneficios del proceso digital.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m4 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/smart_add.png">                                    
                                </div>
                                <div class="card-content">
                                    <img src="assets/img/smart_add_name.png" width="100%">
                                    <p class="justify-align content_beauty">
                                    Una tecnología especialmente diseñada para ofrecer mayor comodidad y relajación visual a los pacientes que en concreto hacen uso de dispositivos digitales.<br><br>
                                    Las zonas de intermedio y cerca se optimizan para que el paciente sienta gran facilidad y comodidad a la hora de enfocar. El ojo está más relajado y precisa menos esfuerzo. Además, ayuda a mantener una postura más saludable y reducir los movimientos de cabeza del paciente.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="a col s12 scale-transition scale-out">
                            <h4>Parámetros de personalización</h4>
                            <p class="text-data-b">
                            Parámetros necesarios <span class="idea_color">para todos los lentes</span>.
                            </p>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_001.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">PRESCRIPCIÓN & ADICIÓN</span>
                                    <p class="justify-align content_beauty">
                                    Digital Ray-Path® calcula la potencia que el usuario realmente percibe una vez montados los lentes en el armazón.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_002.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">DISTANCIA NASOPUPILAR</span>
                                    <p class="justify-align content_beauty">
                                    Se define como la distancia desde el eje simétrico facial hasta el centro de cada pupila.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_003.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">ALTURAS PUPILARES</span>
                                    <p class="justify-align content_beauty">
                                    Es la distancia vertical entre el centro de la pupila y el borde inferior del armazón.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_004.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">DIMENSIONES DEL ARMAZÓN</span>
                                    <p class="justify-align content_beauty">
                                    Las dimensiones del armazón se usan para una optimización más precisa y una mejora del espesor del lente final.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="a col s12 scale-transition scale-out">
                            <h4>Parámetros adicionales</h4>
                            <p class="text-data-b">
                            <span class="idea_color">Para una personalización completa</span>.
                            </p>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_005.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">ÁNGULO PANTOSCÓPICO</span>
                                    <p class="justify-align content_beauty">
                                    Ángulo en el plano vertical entre el eje óptico del lente y el eje visual del ojo en posición primaria.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_006.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">ÁNGULO FACIAL</span>
                                    <p class="justify-align content_beauty">
                                    Curvatura del armazón.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_007.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">DISTANCIA DE VÉRTICE</span>
                                    <p class="justify-align content_beauty">
                                    Es la distancia entre la córnea y la cara interna del lente.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12 m3 b scale-transition scale-out">
                            <div class="card z-depth-0">
                                <div class="card-image">
                                    <img src="assets/img/foto_008.png">                                    
                                </div>
                                <div class="card-content">
                                    <span class="card-title">DISTANCIA DE TRABAJO EN CERCA</span>
                                    <p class="justify-align content_beauty">
                                    Es la distancia desde el lente hasta la posición de lectura habitual del paciente.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col s12">
                            <ul class="tabs principal_brands">
                                <li class="tab col s3"><a class="active" href="#hola1"><img src="assets/img/anatomic_name.png" height="20" /></a></li>
                                <li class="tab col s3"><a href="#hola2"><img src="assets/img/contrast_name.png" height="20" /></a></li>
                                <li class="tab col s3"><a href="#hola3"><img src="assets/img/sensorial_name.png" height="20" /></a></li>
                                <li class="tab col s3"><a href="#hola4"><img src="assets/img/definition_name.png" height="20" /></a></li>
                            </ul>
                        </div>
                        <div id="hola1" class="col s12">
                            <div class="col s12 m7 beuty_stext">
                                <h4 class="card_stitle anatomic">Refract Anatomic</h4>
                                <h5 class="card_sbtitle anatomic">Buen equilibrio en un lente de contacto</h5>
                            </div>
                            <div class="col s12 m7 beuty_stext">
                                <b>Refract Anatomic</b> es perfecto para satisfacer las necesidades visuales que los pacientes demandan en su día a día además de un lente asequible.<br><br>
                                Gracias a su buen balance entre el campo de intermedio, lejos y cerca, este lente se convierte en una solución que ofrece una correcta visión; similar a la de un progresivo convencional pero con las ventajas del proceso digital.<br><br>
                                <div class="col s12 logo_action anatomic_color xs center-align">
                                    <img src="assets/img/anatomic_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m5">
                                <img src="assets/img/person_anatomic.png" width="100%" />
                            </div>
                            <div class="col s12 m7">
                                <img src="assets/img/anatomic_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 m5 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress anatomic_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress anatomic_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress anatomic_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="col s12 center-align">
                                    <img src="assets/img/surface_power_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m7">
                                <h5 class="card_sbtitle anatomic">Beneficios</h5>
                                <ul class="benefits_list">
                                    <li>Lente digital económico que ofrece buena calidad visual.</li>
                                    <li>Lente equilibrado en todas las zonas de visión.</li>
                                    <li>Buenos campos de lejos y cerca.</li>
                                    <li>No necesita la toma de parámetros adicionales.</li>
                                </ul>
                                <span class="card_sbtitle anatomic">Altura mínima de montaje: <b>14 y 18 mm</b></span>
                            </div>
                            <div class="col s12 m5 target anatomic">
                                <h5 class="card_sbtitle anatomic">Target:</h5>
                                <p>Ideal para pacientes que buscan una opción económica con un buen rendimiento.</p>
                            </div>
                        </div>
                        <div id="hola2" class="col s12">
                            <div class="col s12 m7 beuty_stext">
                                <h4 class="card_stitle contrast">Refract Contrast</h4>
                                <h5 class="card_sbtitle contrast">Para las necesidades de los pacientes présbitas</h5>
                            </div>
                            <div class="col s12 m7 beuty_stext">
                                <b>Refract Anatomic</b> es el lente digital económico de uso genérico que ofrece un perfecto equilibrio entre las diferentes zonas de visión y permite, además, una adaptación inmediata a cualquier entorno.<br><br>
                                Aporta una calidad visual similar a un progresivo convencional pero con los beneficios del proceso digital.<br><br>
                                <div class="col s12 logo_action contrast_color xs center-align">
                                    <img src="assets/img/contrast_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m5">
                                <img src="assets/img/person_contrast.png" width="100%" />
                            </div>
                            <div class="col s12 m7">
                                <img src="assets/img/contrast_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 m5 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress contrast_color">
                                            <div class="determinate" style="width: 80%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress contrast_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress contrast_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="col s12 center-align">
                                    <img src="assets/img/surface_power_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m7">
                                <h5 class="card_sbtitle contrast">Beneficios</h5>
                                <ul class="benefits_list">
                                    <li>Buen equilibrio entre las diferentes zonas de visión.</li>
                                    <li>Amplia zona de lejos y cerca.</li>
                                    <li>Buen rendimiento para un uso estándar.</li>
                                    <li>Inset variable: automático y manual.</li>
                                </ul>
                                <span class="card_sbtitle contrast">Altura mínima de montaje: <b>14, 16, 18 y 20 mm</b></span>
                            </div>
                            <div class="col s12 m5 target contrast">
                                <h5 class="card_sbtitle contrast">Target:</h5>
                                <p>Dirigido a usuarios expertos en el uso de lentes progresivos que buscan una solución económica con campos de visión amplios y estables.</p>
                            </div>
                        </div>
                        <div id="hola3" class="col s12">
                            <div class="col s12 m7 beuty_stext">
                                <h4 class="card_stitle sensorial">Refract Sensorial</h4>
                                <h5 class="card_sbtitle sensorial">Diseño superior progresivo de uso diario</h5>
                            </div>
                            <div class="col s12 m7 beuty_stext">
                                <b>Refract Sensorial</b> es un diseño de lentes semipersonalizado y desarrollado con la tecnología Digital Ray-Path® que ofrece un buen equilibrio entre los campos visuales de lejos y cerca combinando buena calidad visual y confort.<br><br>
                                Un lente efectivo a cualquier distancia que aporta generosos campos visuales y claridad de visión en todas las distancias.<br><br>
                                <div class="col s12 logo_action sensorial_color xs center-align">
                                    <img src="assets/img/sensorial_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m5">
                                <img src="assets/img/person_sensorial.png" width="100%" />
                            </div>
                            <div class="col s12 m7">
                                <img src="assets/img/sensorial_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 m5 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress sensorial_color">
                                            <div class="determinate" style="width: 75%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress sensorial_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress sensorial_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="col s12 center-align">
                                    <img src="assets/img/digital_ray_path_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m7">
                                <h5 class="card_sbtitle sensorial">Beneficios</h5>
                                <ul class="benefits_list">
                                    <li>Amplio campo de cerca junto con una generosa zona de visión lejana.</li>
                                    <li>Buena calidad de visión en todas las direcciones de mirada.</li>
                                    <li>Astigmatismo oblicuo minimizado.</li>
                                    <li>Adaptable a cualquier tipo de armazón.</li>
                                </ul>
                                <span class="card_sbtitle sensorial">Altura mínima de montaje: <b>14, 15, 16, 17, 18, 19 y 20 mm</b></span>
                            </div>
                            <div class="col s12 m5 target sensorial">
                                <h5 class="card_sbtitle sensorial">Target:</h5>
                                <p>Específicamente desarrollado para pacientes con experiencia previa en progresivos que demanden un lente de uso diario.</p>
                            </div>
                        </div>
                        <div id="hola4" class="col s12">
                            <div class="col s12 m7 beuty_stext">
                                <h4 class="card_stitle definition">Refract Definition</h4>
                                <h5 class="card_sbtitle definition">Excelente diseño de inmediata adaptación</h5>
                            </div>
                            <div class="col s12 m7 beuty_stext">
                                <b>Refract Definition</b> es un diseño totalmente personalizado que optimiza el equilibrio entre las distancias de cerca y lejos ofreciendo a los pacientes campos visuales más anchos en todas las distancias visuales.<br><br>
                                Desarrollado con Digital Ray-Path®, es la combinación perfecta entre calidad y confort.<br><br>
                                <div class="col s12 logo_action definition_color xs center-align">
                                    <img src="assets/img/definition_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m5">
                                <img src="assets/img/person_definition.png" width="100%" />
                            </div>
                            <div class="col s12 m7">
                                <img src="assets/img/definition_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 m5 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress definition_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress definition_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress definition_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="col s12 center-align">
                                    <img src="assets/img/digital_ray_path_name.png" width="200px" />
                                </div>
                            </div>
                            <div class="col s12 m7">
                                <h5 class="card_sbtitle definition">Beneficios</h5>
                                <ul class="benefits_list">
                                    <li>Progresivo totalmente personalizado para un uso diario.</li>
                                    <li>Excelente definición gracias a la reducción de las aberraciones oblicuas.</li>
                                    <li>Adaptación prácticamente inmediata.</li>
                                    <li>Adaptable a cualquier tipo de armazón.</li>
                                </ul>
                                <span class="card_sbtitle definition">Altura mínima de montaje: <b>14, 15, 16, 17, 18, 19 y 20 mm</b></span>
                            </div>
                            <div class="col s12 m5 target definition">
                                <h5 class="card_sbtitle definition">Target:</h5>
                                <p>Ideal para pacientes exigentes que buscan unos lentes de alta gama con campos visuales amplios y estables.</p>
                            </div>
                        </div>

                        <div class="a col s12 scale-transition scale-out">
                            <h4>Compare lentes Refract</h4>
                            <p class="text-data-b">
                            Vea en resumen lo que <span class="idea_color">Refract</span> tiene para usted.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comparator row">
                <div class="col s12 m3 b scale-transition scale-out">
                    <div class="card-panel refract_product anatomic">
                        <div class="row">
                            <div class="col s12 product_name center-align">
                                <img src="assets/img/anatomic_name.png" width="80%" />
                            </div>
                            <div class="col s12 product_image">
                                <img src="assets/img/anatomic_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 base_color anatomic_color">
                                Ideal como primera opción de lentes multifocales progresivos
                            </div>
                            <div class="col s12 content_product">
                                <b>Para pacientes que están usando lentes de visión sencilla o bifocales.</b> Este diseño tiene menor astigmatismo residual periférico, ofreciendo campos visuales amplios, dando RX más exactas.
                            </div>
                            <div class="col s12 logo_action anatomic_color">
                                <img src="assets/img/surface_power_name.png" width="100%" />
                            </div>

                            <div class="col s12 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress anatomic_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress anatomic_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress anatomic_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m3 b scale-transition scale-out">
                    <div class="card-panel refract_product contrast">
                        <div class="row">
                            <div class="col s12 product_name center-align">
                                <img src="assets/img/contrast_name.png" width="80%" />
                            </div>
                            <div class="col s12 product_image">
                                <img src="assets/img/contrast_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 base_color contrast_color">
                                Para todas las necesidades de los pacientes présbitas
                            </div>
                            <div class="col s12 content_product">
                                <b>Permite corregir errores oblicuos astigmáticos y de potencia.</b> Brinda un desempeño óptico más consistente a través del rango de potencia e incluye <b>selección automática</b> basada en los datos del armazón seleccionado y las medidas particulares de cada paciente.
                            </div>
                            <div class="col s12 logo_action contrast_color">
                                <img src="assets/img/surface_power_name.png" width="100%" />
                            </div>
                            <div class="col s12 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress contrast_color">
                                            <div class="determinate" style="width: 80%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress contrast_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress contrast_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m3 b scale-transition scale-out">
                    <div class="card-panel refract_product sensorial">
                        <div class="row">
                            <div class="col s12 product_name center-align">
                                <img src="assets/img/sensorial_name.png" width="80%" />
                            </div>
                            <div class="col s12 product_image">
                                <img src="assets/img/sensorial_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 base_color sensorial_color">
                                Diseño superior progresivo
                            </div>
                            <div class="col s12 content_product">
                            Permite un <b>perfil de potencia media muy suave con campos de visión más amplios.</b> Reduce los efectos de salto de imagen, para mayor comodidad y una <b>fácil adaptación para el paciente.</b> Zona de astigmatismo reducida hasta en un 50% comparado con diseños convencionales.
                            </div>
                            <div class="col s12 logo_action sensorial_color">
                                <img src="assets/img/digital_ray_path_name.png" width="100%" />
                            </div>
                            <div class="col s12 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress sensorial_color">
                                            <div class="determinate" style="width: 75%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress sensorial_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress sensorial_color">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col s12 m3 b scale-transition scale-out">
                    <div class="card-panel refract_product definition">
                        <div class="row">
                            <div class="col s12 product_name center-align">
                                <img src="assets/img/definition_name.png" width="80%" />
                            </div>
                            <div class="col s12 product_image">
                                <img src="assets/img/definition_aberration.png" width="100%" />
                            </div>
                            <div class="col s12 base_color definition_color">
                                Excelente diseño de inmediata adaptación
                            </div>
                            <div class="col s12 content_product">
                                <b>Excelente para cualquier tipo de ametropía</b>, ejes oblicuos, astigmatismos altos. Ofrece mayor confort y comodidad eliminando el efecto de <i>balanceo</i>.
                            </div>
                            <div class="col s12 logo_action definition_color">
                                <img src="assets/img/digital_ray_path_name.png" width="100%" />
                            </div>
                            <div class="col s12 comparator">
                                <ul class="collection">
                                    <li class="collection-item">
                                        Visión <b>cercana</b>
                                        <div class="progress definition_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        Visión <b>lejana</b>
                                        <div class="progress definition_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                    <li class="collection-item">
                                        <b>Confort</b>
                                        <div class="progress definition_color">
                                            <div class="determinate" style="width: 90%"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="container">
                <div class="row">
                    <div class="content_b">
                        <!-- Definition Drive -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle definition">Refract Definition Drive</h4>
                            <h5 class="card_sbtitle definition">Perfecto para una conducción tranquila y agradable de día y de noche</h5>
                        </div>
                        <div class="col s12 m7 beuty_stext">
                            Lente personalizado diseñado específicamente para la conducción gracias a su distribución de potencia. Gracias a la zona de visión nocturna que incluye, la diferencia de error de refracción que se produce entre el día y la noche (hasta 0,25 D) se ve compensada, lo que proporciona al usuario una gran agudeza visual y una reducción del estrés y la fatiga visual, tan comunes durante la conducción nocturna.<br><br>
                            <div class="col s12 logo_action definition_color xs center-align">
                                <img src="assets/img/brand_definition_drive.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_drive.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_drive.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 70%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 85%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle definition">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Campo visual intermedio ampliado hasta un 45%.</li>
                                <li>Aumento de la comodidad durante la conducción.</li>
                                <li>Reducción de los efectos de la miopía nocturna y el astigmatismo oblicuo.</li>
                                <li>Visión clara en todas las direcciones de la mirada.</li>
                            </ul>
                            <span class="card_sbtitle definition">Altura mínima de montaje: <b>18 mm</b></span><br>
                            * También disponible en opción monofocal
                        </div>
                        <div class="col s12 m5 target definition">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Conductores que demandan un lente progresivo personalizado premium.</p>
                        </div>
                        <!-- Definition Sport -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle definition">Refract Definition Sport</h4>
                            <h5 class="card_sbtitle definition">Lente lenticular ideal para actividades en exteriores</h5>
                        </div>
                        <div class="col s12 m7 beuty_stext">
                            A la hora de realizar actividades en el exterior los requerimientos visuales cambian. Los armazones deportivos suelen ser más grandes y curvados por lo que los lentes tienen más espesor que en armazones normales.<br><br>
                            Con Refraction Definition Sport esto ya no supone una limitación ya que ofrece unos lentes más finos y estéticos adaptables a cualquier tipo de armazón ideales para actividades al aire libre o deportivas.<br><br>
                            <div class="col s12 logo_action definition_color xs center-align">
                                <img src="assets/img/brand_definition_sport.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_sport.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_sport.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 70%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 85%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle definition">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Pasillo amplio que proporciona una visión intermedia cómoda.</li>
                                <li>Reducción de los valores de cilindros laterales no deseados.</li>
                                <li>Visión de cerca ajustada para una visión clara de los accesorios deportivo (mapas, brújula, reloj...).</li>
                                <li>Posición más ergonómica de la cabeza y del cuerpo.</li>
                            </ul>
                            <span class="card_sbtitle definition">Altura mínima de montaje: <b>16 mm y 18 mm</b></span><br>
                        </div>
                        <div class="col s12 m5 target definition">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Diseñado para usuarios que necesitan lentes deportivos en armazones curvados y están limitados a causa de su prescripción.</p>
                        </div>
                        <!-- Definition Sport -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle acomoda">Acomoda</h4>
                            <h5 class="card_sbtitle acomoda">Lente innovadora que reduce significativamente la fatiga fisual</h5>
                        </div>
                        <div class="col s12 beuty_stext">
                            <div class="progress definition_color rainboll">
                                <div class="determinate" style="width: 100%"></div>
                            </div>
                        </div>
                        
                        <div class="col s12 m7 beuty_stext">
                            Lente antifatiga específicamente desarrollado para todos aquellos usuarios no présbitas que sufren de fatiga visual debido a la continua visualización de objetos a distancias cercanas.<br><br>
                            Este lente, el cual incorpora la tecnología Digital Ray-Path® y la tecnología Smart Add, ofrece al usuario una mayor calidad visual, facilita el enfoque y la lectura más rápida de pequeños detalles en las pantallas digitales.<br><br>
                            <div class="col s12 logo_action definition_color xs acomoda center-align">
                                <img src="assets/img/brand_definition_acomoda.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_acomoda.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_acomoda.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 0%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 0%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" /><br>
                                <img src="assets/img/smart_add_name.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle rainboll">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Diseño totalmente personalizado y optimizado para pantallas digitales.</li>
                                <li>Minimiza la fatiga visual.</li>
                                <li>Ofrece excelente calidad visual en cerca.</li>
                                <li>Mejora la ergonomía postural gracias a la reducción de los movimientos de cabeza.</li>
                            </ul>
                            <span class="card_sbtitle rainboll">Altura mínima de montaje: <b>14 mm</b></span><br>
                        </div>
                        <div class="col s12 m5 target rainboll">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Perfecta para usuarios de entre 18 y 45 años que pasan mucho tiempo trabajando en distancias cercanas y sufren comúnmente de fatiga visual.</p>
                        </div>
                        <!-- Definition office -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle acomoda">Refract Office</h4>
                            <h5 class="card_sbtitle acomoda">El lente ocupacional que optimiza las zonas de cerca e intermedio</h5>
                        </div>
                        <div class="col s12 beuty_stext">
                            <div class="progress definition_color rainboll">
                                <div class="determinate" style="width: 100%"></div>
                            </div>
                        </div>
                        
                        <div class="col s12 m7 beuty_stext">
                            Lente ocupacional para distancias intermedia y cerca capaz de aportar un gran confort visual al usuario.<br><br>
                            Goza de unos cambios entre campos muy dinámicos y ágiles, lo que proporciona una mayor agilidad para alternar el enfoque a diferentes distancias, especialmente cuando se trabaja con pantallas digitales.<br><br>
                            <div class="col s12 logo_action definition_color xs acomoda center-align">
                                <img src="assets/img/brand_definition_office.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_office.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_office.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color">
                                        <div class="determinate" style="width: 0%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" /><br>
                                <img src="assets/img/smart_add_name.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle rainboll">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Campo visual de cerca ampliado.</li>
                                <li>Totalmente personalizado y de fácil adaptación.</li>
                                <li>Visión nítidia desde la distancia de lectura hasta 6 metros.</li>
                                <li>Mejora la ergonomía postural gracias a la reducción de los movimientos de cabeza.</li>
                            </ul>
                            <span class="card_sbtitle rainboll">Altura mínima de montaje: <b>14 y 18 mm</b></span><br>
                        </div>
                        <div class="col s12 m5 target rainboll">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Para oficinistas, chefs, pintores y demás présbitas que pasan gran parte de su tiempo trabajando a distancias de trabajo cercanas e intermedias.</p>
                        </div>
                        <!-- Definition blen -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle acomoda">Refract Blen</h4>
                            <h5 class="card_sbtitle acomoda">Lente desarrollado para enfocar a dos áreas diferentes de visión</h5>
                        </div>
                        <div class="col s12 beuty_stext">
                            <div class="progress definition_color rainboll">
                                <div class="determinate" style="width: 100%"></div>
                            </div>
                        </div>
                        
                        <div class="col s12 m7 beuty_stext">
                            Lente digital bifocal con dos partes bien diferenciadas: una parte superior diseñada para la visión de lejos y un segmento circular en la parte inferior para la visión de cerca.<br><br>
                            Asimismo, este diseño se caracteriza por carecer de aberraciones oblicuas, por lo que el usuario se beneficiará de una visión cómoda sin distorsión o efecto de balanceo.<br><br>
                            <div class="col s12 logo_action definition_color xs acomoda center-align">
                                <img src="assets/img/brand_definition_blen.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_blen.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_blen.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 85%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" /><br>
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle rainboll">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Lente bifocal digital totalmente personalizado.</li>
                                <li>Amplio campo de visión de lejos con un segmento curvo en la parte inferior para visión de lectura.</li>
                                <li>Elimina la distorsión y el efecto balanceo.</li>
                                <li>Inset variable y reducción del espesor.</li>
                            </ul>
                            <span class="card_sbtitle rainboll">Altura mínima de montaje: <b>14 mm</b></span><br>
                            El diámetro de este segmento es de 28 mm o 40 mm, con un área de transición de 2.5 mm. La distancia entre la pupila y el segmento es de 3 mm.
                        </div>
                        <div class="col s12 m5 target rainboll">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Ideal para usuarios que ya usan lentes bifocales y quieren, a su vez, disfrutar de amplios campos visuales de cerca y lejos.</p>
                        </div>
                        <!-- Definition dual -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle acomoda">Refract Dual</h4>
                            <h5 class="card_sbtitle acomoda">El lente bifocal digital sin "salto de imagen"</h5>
                        </div>
                        <div class="col s12 beuty_stext">
                            <div class="progress definition_color rainboll">
                                <div class="determinate" style="width: 100%"></div>
                            </div>
                        </div>
                        
                        <div class="col s12 m7 beuty_stext">
                            Lente personalizado diseñado teniendo presente la visión binocular del sistema ojo-lente y el movimiento a través este.<br><br>
                            Optimizado por la tecnología Digital Ray- Path®, su diseño presenta la combinación más precisa de calidad y comodidad para los usuarios de lentes bifocales. Asimismo, este lente es perfecto para todos aquellos usuarios de bifocales que quieran dar el paso hacia un diseño progresivo.<br><br>
                            <div class="col s12 logo_action definition_color xs acomoda center-align">
                                <img src="assets/img/brand_definition_dual.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_dual.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_dual.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 85%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" /><br>
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle rainboll">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Totalmente personalizado y de fácil adaptación.</li>
                                <li>Gran estética ya que elimina la línea divisoria.</li>
                                <li>Solución ideal para pacientes que no se adaptan a los lentes progresivos.</li>
                                <li>Fácil de producir en laboratorio.</li>
                            </ul>
                            <span class="card_sbtitle rainboll">Altura mínima de montaje: <b>15 mm</b></span><br>
                        </div>
                        <div class="col s12 m5 target rainboll">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Perfecto para usuarios de lentes bifocales o para todo aquel que no se ha adaptado a sus lentes progresivos.</p>
                        </div>
                        <!-- Definition hd -->
                        <div class="col s12 beuty_stext">
                            <h4 class="card_stitle acomoda">Refract HD</h4>
                            <h5 class="card_sbtitle acomoda">Excelente nitidez en un lente sin límites</h5>
                        </div>
                        <div class="col s12 beuty_stext">
                            <div class="progress definition_color rainboll">
                                <div class="determinate" style="width: 100%"></div>
                            </div>
                        </div>
                        
                        <div class="col s12 m7 beuty_stext">
                            <b>Refract HD</b> es un monofocal totalmente personalizado gracias a la tecnología de alta gama Digital Ray-Path®, que ofrece un concepto revolucionario de visión sin distorsión desde el centro del lente hasta los bordes. Este lente es especialmente beneficioso en prescripciones altas y negativas.<br><br>
                            <div class="col s12 logo_action definition_color xs acomoda center-align">
                                <img src="assets/img/brand_definition_hd.png" width="200px" />
                            </div>
                        </div>
                        <div class="col s12 m5">
                            <img src="assets/img/person_definition_hd.png" width="100%" />
                        </div>
                        <div class="col s12 m7">
                            <img src="assets/img/aberration_definition_hd.png" width="100%" />
                        </div>
                        <div class="col s12 m5 comparator">
                            <ul class="collection">
                                <li class="collection-item">
                                    Visión <b>cercana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 0%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    Visión <b>lejana</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 0%"></div>
                                    </div>
                                </li>
                                <li class="collection-item">
                                    <b>Confort</b>
                                    <div class="progress definition_color rainboll">
                                        <div class="determinate" style="width: 95%"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col s12 center-align">
                                <img src="assets/img/digital_ray_path_name.png" width="200px" /><br>
                            </div>
                        </div>
                        <div class="col s12 m7">
                            <h5 class="card_sbtitle rainboll">Beneficios</h5>
                            <ul class="benefits_list">
                                <li>Aporta una calidad y confort óptimos para garantizar una visión perfecta.</li>
                                <li>Máxima calidad óptica en cualquier graduación.</li>
                                <li>Compatible con cualquier material y armazón, incluso deportivos o curvados.</li>
                                <li>Lentes más ligeros y finos.</li>
                            </ul>
                            <span class="card_sbtitle rainboll hidden">Altura mínima de montaje: <b>15 mm</b></span><br>
                        </div>
                        <div class="col s12 m5 target rainboll">
                            <h5 class="card_sbtitle definition">Target:</h5>
                            <p>Lente creado para todos los usuarios que necesiten un lente monofocal y especialmente tienen prescripciones altas o corrección astigmática alta.</p>
                        </div>
                        

                        <!-- -->
                    </div>
                </div>
            </div>
        </div>

        <div id="test4" class="col s12">
            <!-- ---------------------------------------------------------------------------------------------------- -->
            <div class="slide_s progresivos_s">
                <div class="slider database">
                    <ul class="slides">
                        <li class="grey darken-4">
                            <img src="assets/img/slider_001.png">
                            <div class="caption center-align">
                                <h3>Lentes progresivos</h3>
                                <h5 class="light grey-text text-lighten-3">Vea bien de tanto de lejos como de cerca</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- /////////////////////////////////////////// -->
                        <div class="b col s12 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/progresivos.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Lentes progresivos</span>
                                    <p class="beauty_product">La superficie óptica progresiva de Varilux® de Essilor ® permite a sus ojos pasar suavemente de visión de lejos a visión de cerca y a todas las distancias intermedias, sin ningún esfuerzo, logrando así que pueda leer el periódico, ver su taza de café y observar a través de la ventana.<br><br>
                                    Contrario a las lentes bifocales, las Lentes Varilux ofrecen una percepción global del espacio y le permiten recuperar su visión natural tal como siempre fue, sin obstáculos o dificultades.</p>
                                </div>
                            </div>
                        </div>
                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image brand_name_c center-align grey darken-3">
                                    <img src="assets/img/logo_des.png">
                                </div>
                                <div class="card-content height_content">
                                    <p class="beauty_product">No hay necesidad de escoger entre estética y desempeño: Varilux S Short provee el desempeño de Varilux S series en armazones pequeños junto con la calidad de Varilux que ya conoces.</p>
                                </div>
                            </div>
                        </div>
                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image brand_name_c center-align grey darken-3">
                                    <img src="assets/img/logo_fit.png">
                                </div>
                                <div class="card-content height_content">
                                    <p class="beauty_product">Varilux s fit es una lente progresiva que forma parte de Varilux S series. Si tienes presbicia, te proporcionarán un campo de visián más amplio con equilibrio en movimiento.</p>
                                </div>
                            </div>
                        </div>
                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image brand_name_c center-align grey darken-3">
                                    <img src="assets/img/logo4d.png">
                                </div>
                                <div class="card-content height_content">
                                    <p class="beauty_product">De la misma forma en que puedes ser zurdo o diestro, tu visión también es dominada por un ojo. Varilux S 4D toma en cuenta al ojo dominante gracias a 4D TECHNOLOGY.</p>
                                </div>
                            </div>
                        </div>
                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image brand_name_c center-align grey darken-3">
                                    <img src="assets/img/physeo.png">
                                </div>
                                <div class="card-content height_content">
                                    <p class="beauty_product">Varilux Physio incorpora la innovadora y exclusiva tecnología WAVE Front, que ha sido utilizada en la cirugía láser para una visión HD para una visión progresiva.</p>
                                </div>
                            </div>
                        </div>
                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image brand_name_c center-align grey darken-3">
                                    <img src="assets/img/comfort.png">
                                </div>
                                <div class="card-content height_content">
                                    <p class="beauty_product">Con el fin de atender a los cambiantes hábitos, Varilux Comfort ha sido rediseñado y optimizado para tomar en cuenta los nuevos requerimientos visuales.</p>
                                </div>
                            </div>
                        </div>
                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image brand_name_c center-align grey darken-3">
                                    <img src="assets/img/liberty.png">
                                </div>
                                <div class="card-content height_content">
                                    <p class="beauty_product">Fabricada mediante un sistema de retallado tradicional, está indicada para no renunciar a la calidad y garantía. Varilux Liberty aporta al usuario facilidad de enfoque en cualquier situación.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /////////////////////////////////////////// -->
                    </div>
                </div>
            </div>
            <!-- ---------------------------------------------------------------------------------------------------- -->
            <div class="slide_s polarizados_s">
                <div class="slider database">
                    <ul class="slides">
                        <li class="grey darken-4">
                            <img src="assets/img/slider_001.png">
                            <div class="caption center-align">
                                <h3>Lentes polarizados</h3>
                                <h5 class="light grey-text text-lighten-3">Proteja sus ojos de los rayos del sol</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- /////////////////////////////////////////// -->
                        <div class="b col s12 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/polarizados.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Lentes polarizados</span>
                                    <p class="beauty_product">Xperio es la nueva marca de lentes polarizadas de Essilor, lider mundial en lentes oftálmicas. Nuestras lentes estan presentes en más de 100 países y son prescritas por profesionales del cuidado de la visión sea cual sea la graduación.Comparadas con las lentes de sol estándar, las lentes polarizadas Xperio marcan la diferencia gracias a sus beneficios:</p>
                                    <ul>
                                        <li>Eliminan los deslumbramientos.</li>
                                        <li>Máximo confort visual.</li>
                                        <li>Claridad de visión sin igual.</li>
                                        <li>Colores más naturales.</li>
                                    </ul>
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/YyylX8ANe1Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <p class="beauty_product"><br><br>Las lentes polarizadas Xperio ofrecen 100% protección UVA/UVB y eliminan los deslumbramiento para una visión clara, cómoda y segura en exteriores. Las Lentes Xperio contienen un film polarizante que separa la luz útil de la perjudicial, eliminando los reflejos que ocasionan el deslumbramiento. Las lentes polarizadas Xperio proporcionan ventajas adicionales sobre las lentes estándar; las gafas de sol estándar reducen el exceso de luz, pero no los deslumbramientos, mientras que las lentes Xperio eliminan por completo los deslumbramientos cegadores a la vez que reducen el exceso de luz.</p>
                                    <img src="assets/img/polarizados_3.png" width="100%">
                                </div>
                            </div>
                        </div>
                        <!-- /////////////////////////////////////////// -->
                    </div>
                </div>
            </div>
            <!-- ---------------------------------------------------------------------------------------------------- -->
            <div class="slide_s fotosensibles_s">
                <div class="slider database">
                    <ul class="slides">
                        <li class="grey darken-4">
                            <img src="assets/img/slider_001.png">
                            <div class="caption center-align">
                                <h3>Lentes fotosensibles</h3>
                                <h5 class="light grey-text text-lighten-3">Lentes que se oscurecen según la luz</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- /////////////////////////////////////////// -->
                        <div class="b col s12 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/fotosensibles.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Lentes fotosensibles</span>
                                    <p class="beauty_product">En Transitions Optical, tratamos de ayudar a las personas a ver la vida con la mejor luz optimizando su visión y la forma en que perciben la luz. Gracias a una innovación constante, los productos Transitions mejoran la visión ajustándose y adaptándose a la luz cambiante. Mediante nuestras asociaciones con los líderes del sector óptico, ofrecemos lentes dinámicas con la mayor selección de diseños y materiales, lo que incluye cientos de combinaciones de lentes. Las lentes Transitions son las lentes fotocrómicas más recomendadas en todo el mundo. Cada segundo, en algún lugar del mundo, un usuario de gafas decide comprar unas lentes dinámicas Transitions. Nos esforzamos sin descanso por llevar la satisfacción del usuario al máximo nivel.</p>
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m4 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/transitions_signature.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Nuestras lentes dinámicas más equilibradas</span>
                                    <p class="beauty_product">Estas lentes están diseñadas para adaptarse rápidamente y pasar de ser claras en interiores a oscurecerse por completo bajo un sol intenso, lo que supone una clara ventaja con respecto a las lentes blancas. Las lentes Transitions Signature se adaptan continuamente a los cambios de luz, de modo que siempre tienen el tono exacto que necesitas.</p>
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m4 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/transitions_xtractive.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Nuestras lentes oscuras, ahora más oscuras</span>
                                    <p class="beauty_product">Especialmente diseñadas para volverse aún más oscuras en exteriores, estas lentes son una estupenda elección si pasas mucho tiempo bajo una luz solar intensa. Las lentes Transitions XTRActive adoptan una cómoda coloración leve en interiores y se activan moderadamente dentro del coche.</p>
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m4 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/transitions_drivewear.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Nuestras lentes oscuras, ideales para conducir</span>
                                    <p class="beauty_product">Las condiciones de conducción cambian con rapidez; las lentes solares Drivewear® Transitions®, también, gracias a que combinan dos de las tecnologías más avanzadas del sector óptico. La polarización NuPolar elimina el resplandor que llega de la carretera y el capó del coche. La tecnología fotocrómica Transitions ajusta el tono y la coloración de las lentes a medida que las condiciones de iluminación varían.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /////////////////////////////////////////// -->
                    </div>
                </div>
            </div>
            <!-- ---------------------------------------------------------------------------------------------------- -->
            <div class="slide_s antireflejantes_s">
                <div class="slider database">
                    <ul class="slides">
                        <li class="grey darken-4">
                            <img src="assets/img/slider_001.png">
                            <div class="caption center-align">
                                <h3>Lentes antireflejantes</h3>
                                <h5 class="light grey-text text-lighten-3">Protección de sus lentes contra rayones y más</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- /////////////////////////////////////////// -->
                        <div class="b col s12 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/antireflejantes.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Lentes antireflejantes</span>
                                    <p class="beauty_product">Las lentes antirreflejantes Crizal® UV son las líderes en el mercado de lentes oftálmicos antirreflejantes. Son usadas por más de 150 millones de personas en el mundo que confían en su calidad y que no las cambiarían por ningunas otras.</p>
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/crizal_prevencia.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Crizal Prevencia UV</span>
                                    <p class="beauty_product">Ahora la gama de Crizal® UV incorpora un producto especializado en protección contra la luz azul-violeta de las pantallas que daña tus ojos: Crizal® Prevencia UV.<br><br></p>
                                    <img src="assets/img/beneficios_prevencia_01.png" width="100%" />
                                    <img src="assets/img/esquema_prevencia21.png" width="100%" />
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/02crizalmx-2000x598_c.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Crizal Forte UV</span>
                                    <p class="beauty_product">Día tras día, la radiación UV tiene efectos nocivos e irreversibles en la piel y en los ojos. Las nuevas lentes antirreflejantes Crizal Forte® UV ofrecen la mejor protección contra los rayos UV que se reflejan en la cara interna de la lente y que afectan a los ojos.<br><br>
                                    Además de la protección contra los nocivos rayos UV, la lente Crizal Forte® UV ofrece el mejor rendimiento para luchar contra los 5 enemigos de la visión, pero se desarrolla a través de una combinación de tecnologías avanzadas que se traducen en los siguientes beneficios.<br><br>
                                        <ol class="beauty_list">
                                            <li>Hasta un 30% menos reflejos que Crizal Alizé® UV (tecnología de optimización antirreflejo).</li>
                                            <li>2 veces más resistente a los rayaduras que Crizal Alizé® UV(Tecnología de arañazos Booster Resistencia).</li>
                                            <li>Ojos hasta 25 veces mejor protegidos contra los rayos UV que sin ningún lente (Broad Spectrum UV Technology).</li>
                                            <li>12 veces más fácil de limpiar que una lente antirreflejante común (tecnologia High Surface Density Process).</li>
                                            <li>Atrae 7 veces menos polvo que cualquier lente antirreflejante común (i-technology).</li>
                                            <li>La mejor repelencia al agua, las gotas se deslizan mucho más rápido que en cualquier otro lente de la gama Crizal® UV. (Slide tecnología Fx TM).</li>
                                        </ol>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/04crizalmx-2000x598_c.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Crizal Alizé UV</span>
                                    <p class="beauty_product">Las lentes antirreflejantes Crizal® Alizé UV combina una excelente resistencia a las manchas y la repelencia al polvo, manteniendo el rendimiento antirreflejante y resistencia al rayas superior.<br><br>
                                        <ol class="beauty_list">
                                            <li>Excelente eficiencia antirreflejante.</li>
                                            <li>Excelente resistencia a las rayaduras.</li>
                                            <li>10 veces más fácil de limpiar que una lente antirreflejante común.</li>
                                            <li>Atrae 7 veces menos polvo que una lente antirreflejante común.</li>
                                            <li>Muy buena repelencia al agua.</li>
                                        </ol>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="b col s12 m6 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/05crizalmx-2000x598_c.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Crizal Easy UV</span>
                                    <p class="beauty_product">Crizal® easy UV es la lente antirreflejante de entrada a la gama Crizal® UV que ofrece una buena resistencia a las rayaduras e igualmente una buena resistencia a las manchas. Una buena relación costo-beneficio con la calidad Crizal® UV.<br><br>
                                        <ol class="beauty_list">
                                            <li>Muy buena eficiencia antirreflejante.</li>
                                            <li>Muy buena resistencia a rayaduras.</li>
                                            <li>8 veces más fácil de limpiar que las lentes normales.</li>
                                            <li>Buena repelencia al agua.</li>
                                        </ol>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /////////////////////////////////////////// -->
                    </div>
                </div>
            </div>
            <!-- ---------------------------------------------------------------------------------------------------- -->
            <div class="slide_s contacto_s">
                <div class="slider database">
                    <ul class="slides">
                        <li class="grey darken-4">
                            <img src="assets/img/slider_001.png">
                            <div class="caption center-align">
                                <h3>Lentes de contacto</h3>
                                <h5 class="light grey-text text-lighten-3">Protección y personalización de sus ojos</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- /////////////////////////////////////////// -->
                        <div class="b col s12 scale-transition scale-out">
                            <div class="card">
                                <div class="card-image">
                                    <img src="assets/img/lentesdecontacto.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title beauty_parent">Lentes de contacto</span>
                                    <p class="center-align">
                                        <img src="assets/img/freshlook.jpg" width="50%" />
                                    </p>
                                    <p class="beauty_product"><br><br>FreshLook ColorBlends® exclusiva tecnología ColorBlends® que combina 3 colores en 1. Tiene una estrella color miel en el centro que se mezcla mejor con el color natural del ojo, más un aro exterior de color gris que le da profundidad. El resultado es una mirada linda y completamente natural.<br><br></p>
                                    <img src="assets/img/colorblends.jpg" width="100%" />
                                </div>
                            </div>
                        </div>
                        <!-- /////////////////////////////////////////// -->
                    </div>
                </div>
            </div>
            <!-- ---------------------------------------------------------------------------------------------------- -->
        </div>

        <div id="test3" class="col s12">
            <div class="slider database">
                <ul class="slides">
                    <li class="grey darken-4">
                        <img src="assets/img/slider_001.png">
                        <div class="caption center-align">
                            <h3>Laboratorio Óptico</h3>
                            <h5 class="light grey-text text-lighten-3">Conozca nuestros procesos y confíe en los expertos</h5>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="container">
                <div class="row">

                    <div class="a col s12 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image center-align">
                                <img src="assets/img/varilux_nombre.png" style="width:50%; display:inline; padding:20px 0;">
                            </div>
                            <div class="card-content">
                                <span class="card-title beauty_parent">Tallado digital</span>
                                <p>Nuestros equipos con tecnología de punta procesan los materiales de una forma mas exacta y precisa para garantizar la maxima calidad en todos sus pedidos.</p>
                            </div>
                        </div>
                    </div>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <img src="assets/img/antireflejante.png">
                            </div>
                            <div class="card-content">
                                <span class="card-title beauty_parent">Antireflejantes</span>
                                <p>Este recubrimiento mejora la capacidad visual frente a diferentes entornos usando la más alta tecnología crizal UV.</p>
                            </div>
                        </div>
                    </div>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-image">
                                <img src="assets/img/antireflejante.png">
                            </div>
                            <div class="card-content">
                                <span class="card-title beauty_parent">Antirayas</span>
                                <p>Este recubrimiento mejora la capacidad visual frente a diferentes entornos usando la más alta tecnología crizal UV.</p>
                            </div>
                        </div>
                    </div>

                    <h4 class="col s12">Materiales de trabajo</h4>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title beauty_parent">CR-39</span>
                                <p class="beauty_product">Lente Organica índice de refracción 1.50 , primer material orgánico en salir al mercado, se caracteriza por una densidad muy baja que le da la ligereza, pero por su índice de refracción bajo hace que las lentes sean más gruesas, su número Abbe es alto por lo cual ópticamente correcto . Se recomienda para armazones completos y graduaciones bajas, ideal para aplicar tintes.</p>
                            </div>
                        </div>
                    </div>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title beauty_parent">Hi index</span>
                                <p class="beauty_product">Lente orgánica índice de refracción 1.56 con valor añadido al CR-39, por su menor curva y densidad, con un número de Abbe más bajo, permite hacer lentes más delgadas y ligeras. Aconsejable para graduaciones altas y armazones completos.</p>
                            </div>
                        </div>
                    </div>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title beauty_parent">Thin & Lite</span>
                                <p class="beauty_product">Lente orgánica, índice de refracción 1.67, su densidad es muy baja, con número Abbe bajo, permite la máxima reducción de espesores. Totalmente recomendado para graduaciones altas y con tratamiento Anti-reflejante.</p>
                            </div>
                        </div>
                    </div>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title beauty_parent">Policarbonato</span>
                                <p class="beauty_product">Es un alto índice, con una densidad inferior al CR-39, con número Abbe bajo, mejora la estética de los lentes reduciendo el grosor y absorbe totalmente la luz U.V. Se caracteriza por su dureza, ideal para armazones de 3 piezas, ranurados y lentes de seguridad industrial.</p>
                            </div>
                        </div>
                    </div>

                    <div class="b col s12 m6 scale-transition scale-out">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title beauty_parent">Cristal</span>
                                <p class="beauty_product">Lestes minerales, con índices de refracción variables de (1.523 – 1.9) principalmente compuestos por Sílice fundido con óxidos metálicos. Presentan una notable dureza y resistencia al rayado, pero son muy pesados por su mayor densidad, se recomiendan para armazones completos.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div id="test6" class="col s12">
            <div class="col s12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1887.9157107547103!2d-97.10157817856135!3d18.850162023596845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c502a5820fb8b1%3A0xb7335155a74b2055!2sStarlab!5e0!3m2!1ses-419!2smx!4v1599502863557!5m2!1ses-419!2smx" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="container">
                <div class="row">
                    <div class="m col s12">
                        <br><br>
                        <h4 class="center-align">Póngase en contacto con nosotros</h4>
                        <h5 class="center-align">Y uno de nuestros representantes se pondrá en contacto con usted.</h5>
                        <br><br>
                    </div>
                    <div class="m col s12 m6">
                        <p class="text-data-b">
                            <b>Starlab - Laboratorios ópticos</b><br>
                            Norte 10 #44 Col. Centro<br>
                            Orizaba, Ver, México, 94300<br>
                            <a href="tel:8007120150">800 712 0150</a><br>
                            <a href="tel:2727241717">272 724 1717</a><br>
                            <a href="tel:2721214360">272 121 4360</a><br>
                            <a href="tel:2721069403">272 106 9403</a><br>
                        </p>
                    </div>
                    <div class="m col s12 m6">
                        <p class="text-data-b">
                            <form class="form1" action="#" validate>
                                <div class="modal-content">
                                    <div class="row">
                                        <div class="col s12 input-field">
                                            <i class="material-icons prefix">account_circle</i>
                                            <input id="first_name2" type="text" class="validate" required>
                                            <label for="first_name2">Nombre completo</label>
                                            <span class="helper-text" data-error="Ingrese su nombre por favor" data-success="Correcto">Ej: Francisco Castillo</span>
                                        </div>
                                        <div class="col s12 input-field">
                                            <i class="material-icons prefix">email</i>
                                            <input id="email2" type="email" class="validate" required>
                                            <label for="email2">Correo electrónico</label>
                                            <span class="helper-text" data-error="Ingrese su correo por favor" data-success="Correcto">star_labori@hotmail.com</span>
                                        </div>
                                        <div class="col s12 input-field">
                                            <i class="material-icons prefix">phone</i>
                                            <input id="phone_number2" type="number" class="validate" required>
                                            <label for="phone_number2">Número telefónico</label>
                                            <span class="helper-text" data-error="Ingrese su teléfono por favor" data-success="Correcto">Ej: 8007120150</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer center-align">
                                    <a href="#!" class="send_meeting waves-effect waves-green btn idea_back">Solicitar informes</a>
                                </div>
                            </form>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <footer class="page-footer idea_back">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Starlab - Laboratorios ópticos</h5>
                    <p class="grey-text text-lighten-4">
                        Norte 10 #44 Col. Centro<br>
                        Orizaba, Ver, México, 94300<br><br>
                        <a class="white-text" href="tel:8007120150">800 712 0150</a><br>
                        <a class="white-text" href="tel:2727241717">272 724 1717</a><br>
                        <a class="white-text" href="tel:2721214360">272 121 4360</a><br>
                        <a class="white-text" href="tel:2721069403">272 106 9403</a><br>
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Accesos</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="mailto:star_labori@hotmail.com">star_labori@hotmail.com</a></li>
                        <li><a class="grey-text text-lighten-3" href="mailto:starlabori@hotmail.com">starlabori@hotmail.com</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://silo.starlab.com.mx">Sistema de pedidos</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2020 Starlab - Laboratorios ópticos
                <a class="grey-text text-lighten-4 right" href="https://www.grupomarssoft.com">Grupo Marssoft</a>
            </div>
        </div>
    </footer>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large idea_back pulse modal-trigger" data-target="modal1">
            <i class="large material-icons">phone</i>
        </a>
    </div>
    <div id="modal1" class="modal">
        <form class="form1" action="#" validate>
            <div class="modal-content">
                <h4>Comuníquese con nosotros</h4>
                <div class="row">
                    <div class="col s12 input-field">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="first_name" type="text" class="validate" required>
                        <label for="first_name">Nombre completo</label>
                        <span class="helper-text" data-error="Ingrese su nombre por favor" data-success="Correcto">Ej: Francisco Castillo</span>
                    </div>
                    <div class="col s12 input-field">
                        <i class="material-icons prefix">email</i>
                        <input id="email" type="email" class="validate" required>
                        <label for="email">Correo electrónico</label>
                        <span class="helper-text" data-error="Ingrese su correo por favor" data-success="Correcto">starlabori@hotmail.com</span>
                    </div>
                    <div class="col s12 input-field">
                        <i class="material-icons prefix">phone</i>
                        <input id="phone_number" type="number" class="validate" required>
                        <label for="phone_number">Número telefónico</label>
                        <span class="helper-text" data-error="Ingrese su teléfono por favor" data-success="Correcto">Ej: 800 712 0150</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-red btn-flat">Cerrar</a>
                <a href="#!" class="send_meeting waves-effect waves-green btn-flat">Solicitar informes</a>
            </div>
        </form>
    </div>

</body>

</html>