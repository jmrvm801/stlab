<?php
if (!isset($_POST['data']))
    echo json_encode(array('response' => 'Fernando needs data to sent email'));
else {
    $data = $_POST['data'];
    $data['first_name'] = utf8_decode($data['first_name']);
    $cuerpo = utf8_decode(' 
	<table width="750" border="0" cellspacing="0" cellpadding="0" style="background:#FFF;font-family:sans-serif;">
        <tr>
            <td align="center">
                <table width="680" border="0" cellspacing="0" cellpadding="0" style="margin:25px;background:#3f51b5;-webkit-border-radius: 6px;-moz-border-radius: 6px;-o-border-radius: 6px;border-radius: 6px;">
                <tr>
                    <td style="padding:15px;font-size:25px;color:#FFF;" align="center">Cliente nuevo</td>
                </tr>
                <tr>
                    <td style="padding:10px 25px;font-size:16px;word-break:break-all;color:#FFF;">
                    NOMBRE: '.$data['first_name'].'<br>
                    TELÉFONO: '.$data['phone_number'].'<br>
                    CORREO: '.$data['email'].'<br>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:10px;font-size: 15px;" align="center">&copy; '.date('Y').' <a href="https://www.grupomarssoft.com" style="color:#3F51B5;font-weight:bold;text-decoration:none;">Grupo Marssoft</a></td>
        </tr>
        </table>
        '); 
        $headers = "MIME-Version: 1.0\r\n"; 
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
        $headers .= "From: Sitio web IDEA de Oriente <informes@ideadeoriente.edu.mx>\r\n"; 
        //return $cuerpo;
        $sent = @mail('informes@ideadeoriente.edu.mx','Mensaje de un cliente',$cuerpo,$headers);
        echo json_encode(array('response' => ($sent) ? 'true' : 'false'));
}
